using System;
using System.Collections.Generic;
using System.Linq;

namespace ModulesFramework
{
    public class DataTransferContainer
    {
        private class ObjectToTypesSearchContainer
        {
            private readonly int[] hashes;
            private readonly int hash;
            private readonly object instance;

            public ObjectToTypesSearchContainer(List<Type> types, object instance)
            {
                this.instance = instance;
                hashes = new int[types.Count];
                for (var i = 0; i < types.Count; i++)
                {
                    hashes[i] = types[i].GetHashCode();
                }

                hash = CombineHashCodes(hashes);
            }

            public T GetInstanceIfTypesMatch<T>(params Type[] types)
            {
                var matches = 0;
                var maxMatches = types.Length;
                for (var i = 0; i < maxMatches; i++)
                {
                    if (hashes.Contains(types[i].GetHashCode()))
                    {
                        matches++;
                        if (matches >= maxMatches)
                        {
                            var casted = (T) instance;

                            if (casted == null)
                            {
                                throw new InvalidCastException();
                            }

                            return casted;
                        }
                    }
                }

                return default(T);
            }

            public override int GetHashCode()
            {
                return hash;
            }

            private static int CombineHashCodes(params int[] hashCodes)
            {
                if (hashCodes == null)
                {
                    throw new ArgumentNullException(nameof(hashCodes));
                }

                if (hashCodes.Length == 0)
                {
                    throw new IndexOutOfRangeException();
                }

                if (hashCodes.Length == 1)
                {
                    return hashCodes[0];
                }

                var result = hashCodes[0];

                for (var i = 1; i < hashCodes.Length; i++)
                {
                    result = CombineHashCodes(result, hashCodes[i]);
                }

                return result;
            }

            private static int CombineHashCodes(int h1, int h2)
            {
                return ((h1 << 5) + h1) ^ h2;
            }
        }

        private readonly HashSet<ObjectToTypesSearchContainer> containers = new HashSet<ObjectToTypesSearchContainer>();

        public DataTransferContainer(params object[] objects)
        {
            for (var i = 0; i < objects.Length; i++)
            {
                var obj = objects[i];
                if (obj == null)
                {
                    UnityEngine.Debug.LogError($"Trying to pack null value at {i} index!");
                }

                PackData(obj);
            }
        }

        public T ExtractData<T>(params Type[] filter)
        {
            if (filter.Length == 0)
            {
                filter = new[] {typeof(T)};
            }

            foreach (var container in containers)
            {
                var instance = container.GetInstanceIfTypesMatch<T>(filter);
                if (!Equals(instance, default(T)))
                    return instance;
            }

            return default(T);
        }

        private void PackData(object obj)
        {
            var types = new List<Type>();
            GetAllParentTypes(obj.GetType(), types);
            containers.Add(new ObjectToTypesSearchContainer(types, obj));
        }

        private static void GetAllParentTypes(Type type, List<Type> list)
        {
            if (type == null || list.Contains(type))
            {
                return;
            }

            GetAllParentTypes(type.BaseType, list);

            foreach (var t in type.GetInterfaces())
            {
                GetAllParentTypes(t, list);
            }

            list.Add(type);
        }
    }
}