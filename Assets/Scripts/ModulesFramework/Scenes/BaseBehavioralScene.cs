using System;
using UnityEngine.SceneManagement;

namespace ModulesFramework.Scenes
{
    public abstract class BaseBehavioralScene : ILoadableModule<SceneLoadData>
    {
        public Scene? SceneHandle { get; set; }

        public virtual void OnModuleLoaded(SceneLoadData data, Action callback)
        {
            callback?.Invoke();
        }

        public virtual void OnBeforeModuleUnloaded(Action callback)
        {
            callback?.Invoke();
        }

        public abstract void SetBridge(CoreToBehavioursBridge bridge);
    }
}