namespace ModulesFramework.Scenes
{
    public interface ISceneRegistrationData
    {
        BaseBehavioralScene GetSceneInstance();
        string GetSceneName();
    }
}