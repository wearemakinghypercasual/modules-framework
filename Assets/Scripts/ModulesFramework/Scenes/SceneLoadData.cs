using UnityEngine.SceneManagement;

namespace ModulesFramework.Scenes
{
    public class SceneLoadData : BaseModuleLoadData
    {
        public LoadSceneMode loadSceneMode;
        public bool makeActive;

        public SceneLoadData(LoadSceneMode loadSceneMode = LoadSceneMode.Additive,
            bool makeActive = false, GCType garbageCollectionType = GCType.BeforeLoad,
            bool reloadIfAlreadyLoaded = false)
        {
            this.garbageCollectionType = garbageCollectionType;
            this.loadSceneMode = loadSceneMode;
            this.makeActive = makeActive;
            this.reloadIfAlreadyLoaded = reloadIfAlreadyLoaded;
        }
    }
}