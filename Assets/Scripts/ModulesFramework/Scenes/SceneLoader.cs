using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ModulesFramework.Scenes
{
    public class SceneLoader : ModuleLoader<BaseBehavioralScene, SceneLoadData>
    {
        private readonly Dictionary<string, Stack<Action<Scene>>> onLoadedCallbacks =
            new Dictionary<string, Stack<Action<Scene>>>();

        private readonly Dictionary<string, Stack<Action<Scene>>>
            onUnloadedCallbacks = new Dictionary<string, Stack<Action<Scene>>>();
        private readonly List<ISceneRegistrationData> sceneRegistrationData;

        public SceneLoader(List<ISceneRegistrationData> sceneRegistrationData)
        {
            this.sceneRegistrationData = sceneRegistrationData;
            foreach (var registrationData in sceneRegistrationData)
            {
                RegisterModule(registrationData.GetSceneInstance());
            }

            SceneManager.sceneLoaded += OnUnitySceneLoaded;
            SceneManager.sceneUnloaded += OnUnitySceneUnloaded;
        }

        protected override void Load(BaseBehavioralScene module, SceneLoadData data, Action callback)
        {
            var suitableModule =
                sceneRegistrationData.FirstOrDefault(x => x.GetSceneInstance().GetType() == module.GetType());
            if (suitableModule == null)
            {
                Debug.LogError("Suitable module not found!");
                return;
            }

            var unitySceneName = suitableModule.GetSceneName();
            AddSceneEventListener(onLoadedCallbacks, unitySceneName, x =>
            {
                module.SceneHandle = x;
                var behaviour = FindSceneBridge(x);
                if (behaviour == null)
                {
                    Debug.LogError($"There is no bridge in {unitySceneName}!");
                }

                module.SetBridge(behaviour);
                behaviour.StartCoroutine(RaiseLoadCallback(x, data.makeActive, callback));
            });

            SceneManager.LoadSceneAsync(unitySceneName, data.loadSceneMode);
        }

        protected override void Unload(BaseBehavioralScene module, Action callback)
        {
            if (module.SceneHandle == null) throw new NullReferenceException();
            var name = module.SceneHandle.Value.name;
            AddSceneEventListener(onUnloadedCallbacks, name, x =>
            {
                module.SceneHandle = null;
                callback();
            });

            SceneManager.UnloadSceneAsync(name);
        }

        private void AddSceneEventListener(IDictionary<string, Stack<Action<Scene>>> callbacks, string sceneName,
            Action<Scene> listener)
        {
            if (callbacks.ContainsKey(sceneName))
            {
                callbacks[sceneName].Push(listener);
            }
            else
            {
                var newStack = new Stack<Action<Scene>>();
                newStack.Push(listener);
                callbacks.Add(sceneName, newStack);
            }
        }

        private void FireSceneEvent(IDictionary<string, Stack<Action<Scene>>> callbacks, Scene sceneHandle)
        {
            if (!callbacks.TryGetValue(sceneHandle.name, out var callsStack)) return;
            while (callsStack.Count > 0)
            {
                callsStack.Pop().Invoke(sceneHandle);
            }
        }

        private void OnUnitySceneLoaded(Scene scene, LoadSceneMode loadMode)
        {
            FireSceneEvent(onLoadedCallbacks, scene);
        }

        private void OnUnitySceneUnloaded(Scene scene)
        {
            FireSceneEvent(onUnloadedCallbacks, scene);
        }

        private CoreToBehavioursBridge FindSceneBridge(Scene scene)
        {
            var objects = scene.GetRootGameObjects();
            return objects.Select(obj => obj.GetComponentInChildren<CoreToBehavioursBridge>())
                .FirstOrDefault(sceneBehaviour => sceneBehaviour != null);
        }

        private static IEnumerator RaiseLoadCallback(Scene scene, bool makeActive, Action callback)
        {
            yield return null;
            if (makeActive)
            {
                SceneManager.SetActiveScene(scene);
            }

            callback();
        }
    }
}