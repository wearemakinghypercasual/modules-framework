namespace ModulesFramework.Scenes
{
    public abstract class GenericSceneCore<T> : BaseBehavioralScene where T : CoreToBehavioursBridge
    {
        protected T coreToBehaviourBridge;

        public override void SetBridge(CoreToBehavioursBridge behaviour)
        {
            coreToBehaviourBridge = behaviour as T;
        }
    }
}