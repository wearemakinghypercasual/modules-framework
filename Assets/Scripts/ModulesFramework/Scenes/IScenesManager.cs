using System;

namespace ModulesFramework.Scenes
{
    public interface IScenesManager
    {
        void Load(ITypeConstraint<BaseBehavioralScene> sceneType, SceneLoadData data, Action callback);
        void Unload(ITypeConstraint<BaseBehavioralScene> sceneType, Action onUnloaded);
        T GetLoadedSceneModule<T>() where T : BaseBehavioralScene;
    }
}