using System;
using System.Collections.Generic;

namespace ModulesFramework.Scenes
{
    public class ScenesManager : IScenesManager
    {
        private readonly ModuleLoader<BaseBehavioralScene, SceneLoadData> sceneLoader;

        public ScenesManager(List<ISceneRegistrationData> baseBehavioralScenes)
        {
            sceneLoader = new SceneLoader(baseBehavioralScenes);
        }

        public void Load(ITypeConstraint<BaseBehavioralScene> sceneType, SceneLoadData data, Action callback)
        {
            sceneLoader.Load(sceneType.GetContainingType(), data, callback);
        }

        public void Unload(ITypeConstraint<BaseBehavioralScene> sceneType, Action onUnloaded)
        {
            sceneLoader.Unload(sceneType.GetContainingType(), onUnloaded);
        }

        public T GetLoadedSceneModule<T>() where T : BaseBehavioralScene
        {
            if (sceneLoader.GetInstancedModule(typeof(T), out var module))
                return module as T;
            return null;
        }
    }
}