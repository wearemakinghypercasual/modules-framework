namespace ModulesFramework.Scenes
{
    public class SceneRegistrationData<T> : ISceneRegistrationData where T : BaseBehavioralScene
    {
        private readonly T sceneInstance;
        private readonly string unitySceneName;

        public SceneRegistrationData(T sceneInstance, string unitySceneName)
        {
            this.sceneInstance = sceneInstance;
            this.unitySceneName = unitySceneName;
        }

        public BaseBehavioralScene GetSceneInstance()
        {
            return sceneInstance;
        }

        public string GetSceneName()
        {
            return unitySceneName;
        }
    }
}