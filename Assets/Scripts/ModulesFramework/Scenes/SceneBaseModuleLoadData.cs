using UnityEngine.SceneManagement;

namespace ModulesFramework.Scenes
{
    public class SceneBaseModuleLoadData : BaseModuleLoadData
    {
        public readonly LoadSceneMode loadSceneMode;
        public readonly string unitySceneName;
        public readonly bool makeActive;

        public SceneBaseModuleLoadData(string unitySceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Additive,
            bool makeActive = false, GCType garbageCollectionType = GCType.BeforeLoad)
        {
            this.garbageCollectionType = garbageCollectionType;
            this.loadSceneMode = loadSceneMode;
            this.unitySceneName = unitySceneName;
            this.makeActive = makeActive;
        }
    }
}