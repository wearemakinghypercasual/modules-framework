using System;

namespace ModulesFramework
{
    public interface ILoadableModule<in T> where T : BaseModuleLoadData
    {
        void OnModuleLoaded(T data, Action callback);
        void OnBeforeModuleUnloaded(Action callback);
    }
}