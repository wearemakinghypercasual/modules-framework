namespace ModulesFramework.UI
{
    public class UIModuleRegistrationData
    {
        public UIModule ModuleInstance { get; }
        public string ResourceId { get; }

        public UIModuleRegistrationData(UIModule moduleInstance, string resourceId)
        {
            ResourceId = resourceId;
            ModuleInstance = moduleInstance;
        }

        public static UIModuleRegistrationData Create<TModuleType>(TModuleType moduleInstance,
            string resourceId)
            where TModuleType : UIModule
        {
            return new UIModuleRegistrationData(moduleInstance, resourceId);
        }
    }
}