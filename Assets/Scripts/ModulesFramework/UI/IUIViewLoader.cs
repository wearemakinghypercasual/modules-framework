using System;
using UnityEngine;

namespace ModulesFramework.UI
{
    public interface IUIViewLoader
    {
        UIModuleView CreateInstancePrioritized(Type moduleType,
            Transform parent);
    }
}