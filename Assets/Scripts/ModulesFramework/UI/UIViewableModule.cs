namespace ModulesFramework.UI
{
    public abstract class UIViewableModule<T> : UIModule where T : UIModuleView
    {
        protected T view;

        public override void SetView(UIModuleView uiView)
        {
            view = uiView as T;
        }

        public override UIModuleView GetView()
        {
            return view;
        }
    }
}