using System;

namespace ModulesFramework.UI
{
    public abstract class UIModule : ILoadableModule<UIModuleLoadData>
    {
        public virtual void OnModuleLoaded(UIModuleLoadData data, Action callback)
        {
            callback?.Invoke();
        }

        public virtual void OnBeforeModuleUnloaded(Action callback)
        {
            callback?.Invoke();
        }

        public abstract void SetView(UIModuleView uiView);
        public abstract UIModuleView GetView();
    }
}