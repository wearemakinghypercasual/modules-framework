using System;
using Object = UnityEngine.Object;

namespace ModulesFramework.UI
{
    public class UIModuleLoader : ModuleLoader<UIModule, UIModuleLoadData>
    {
        private readonly IUIViewLoader uiViewLoader;

        public UIModuleLoader(IUIViewLoader uiViewLoader)
        {
            this.uiViewLoader = uiViewLoader;
        }

        protected override void Load(UIModule module, UIModuleLoadData data, Action callback)
        {
            var uiModuleView = uiViewLoader.CreateInstancePrioritized(module.GetType(), data.RootTransform);
            uiModuleView.OnLinkToModule();
            module.SetView(uiModuleView);
            callback();
        }

        protected override void Unload(UIModule module, Action callback)
        {
            Object.Destroy(module.GetView().gameObject);
            module.SetView(null);
            callback?.Invoke();
            //CoroutineManager.Instance.StartInNextFrame(callback);
        }
    }
}