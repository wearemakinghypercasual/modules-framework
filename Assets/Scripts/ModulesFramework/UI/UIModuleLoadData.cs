using UnityEngine;

namespace ModulesFramework.UI
{
    public class UIModuleLoadData : BaseModuleLoadData
    {
        public Transform RootTransform { get; }

        public UIModuleLoadData(Transform rootTransform,
            GCType garbageCollectionType = GCType.None, bool reloadIfAlreadyLoaded = true)
        {
            this.garbageCollectionType = garbageCollectionType;
            this.reloadIfAlreadyLoaded = reloadIfAlreadyLoaded;
            RootTransform = rootTransform;
        }
    }
}