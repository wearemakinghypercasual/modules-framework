using System;
using UnityEngine;

namespace ModulesFramework.UI
{
    public interface IUIModulesManager
    {
        void Mount(ITypeConstraint<UIModule> moduleType, DataTransferContainer data, Action callback,
            Transform rootTransform = null);

        void Unmount(ITypeConstraint<UIModule> moduleType, Action callback);
        UIModule GetMountedModule(ITypeConstraint<UIModule> moduleType);
        void SetMountRoot(Transform rootTransform);
    }
}