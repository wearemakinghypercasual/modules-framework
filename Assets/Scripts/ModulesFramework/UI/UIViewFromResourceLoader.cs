using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ModulesFramework.UI
{
    public class UIViewFromResourceLoader : IUIViewLoader
    {
        private readonly Dictionary<Type, string> typeToViewResourceMap;
        private readonly List<UIModuleView> loadedViews;

        public UIViewFromResourceLoader(Dictionary<Type, string> typeToViewResourceMap)
        {
            this.typeToViewResourceMap = typeToViewResourceMap;
            loadedViews = new List<UIModuleView>();
        }

        private GameObject LoadUIModulePrefab(string path)
        {
            return Resources.Load<GameObject>($"UIModules/{path}");
        }

        public UIModuleView CreateInstancePrioritized(Type moduleType, Transform parent)
        {
            if (!typeToViewResourceMap.TryGetValue(moduleType, out var resourcePath))
                throw new Exception("Matching view not found!");
            var resource = LoadUIModulePrefab(resourcePath);
            var instance = Object.Instantiate(resource, parent);
            var uiModuleView = instance.GetComponent<UIModuleView>();

            loadedViews.Add(uiModuleView);
            loadedViews.RemoveAll(item => item == null);
            loadedViews.Sort((x, y) => x.Priority.CompareTo(y.Priority));
            instance.transform.SetSiblingIndex(loadedViews.IndexOf(uiModuleView));
            return uiModuleView;
        }
    }
}