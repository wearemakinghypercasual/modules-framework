using UnityEngine;

namespace ModulesFramework.UI
{
    public class UIModuleView : MonoBehaviour
    {
        [SerializeField]
        private int priority;

        public int Priority
        {
            get => priority;
        }

        public virtual void OnLinkToModule()
        {
        }
    }
}