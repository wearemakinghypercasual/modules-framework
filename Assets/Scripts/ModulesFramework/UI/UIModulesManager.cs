using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ModulesFramework.UI
{
    public class UIModulesManager : IUIModulesManager
    {
        private readonly ModuleLoader<UIModule, UIModuleLoadData> uiModuleLoader;
        private Transform mountRoot;
        private readonly List<UIModuleRegistrationData> modulesData;

        public UIModulesManager(Transform mountRoot, List<UIModuleRegistrationData> modulesData)
        {
            this.mountRoot = mountRoot;
            this.modulesData = modulesData;

            var viewLoader =
                new UIViewFromResourceLoader(this.modulesData.ToDictionary(key => key.ModuleInstance.GetType(),
                    val => val.ResourceId));
            uiModuleLoader = new UIModuleLoader(viewLoader);

            foreach (var data in this.modulesData)
            {
                uiModuleLoader.RegisterModule(data.ModuleInstance);
            }
        }

        public T GetMountedModule<T>() where T : UIModule
        {
            if (uiModuleLoader.GetInstancedModule(typeof(T), out var m))
            {
                return m as T;
            }

            return null;
        }

        public UIModule GetMountedModule(ITypeConstraint<UIModule> moduleType)
        {
            return uiModuleLoader.GetInstancedModule(moduleType.GetContainingType(), out var m) ? m : null;
        }

        public void SetMountRoot(Transform rootTransform)
        {
            mountRoot = rootTransform;
        }

        public void Mount(ITypeConstraint<UIModule> moduleType, DataTransferContainer data, Action callback,
            Transform rootTransform = null)
        {
            var moduleData =
                modulesData.FirstOrDefault(x => x.ModuleInstance.GetType() == moduleType.GetContainingType());
            if (moduleData == null)
            {
                Debug.LogError("Module not found!");
                return;
            }

            var mountRootTransform = rootTransform == null ? mountRoot.transform : rootTransform;

            var loadData =
                new UIModuleLoadData(mountRootTransform)
                    {content = data};
            uiModuleLoader.Load(moduleType.GetContainingType(), loadData, callback);
        }

        public void Unmount(ITypeConstraint<UIModule> moduleType, Action callback)
        {
            uiModuleLoader.Unload(moduleType.GetContainingType(), callback);
        }
    }
}