namespace ModulesFramework
{
    public class BaseModuleLoadData
    {
        public DataTransferContainer content;
        public GCType garbageCollectionType;
        public bool reloadIfAlreadyLoaded;

        public enum GCType
        {
            None,
            BeforeLoad,
            AfterLoad
        }
    }
}