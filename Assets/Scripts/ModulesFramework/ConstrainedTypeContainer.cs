using System;

namespace ModulesFramework
{
    /// <summary>
    /// This interface is used to semantically constrain usage of type which is not a covariant of T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITypeConstraint<out T>
    {
        Type GetContainingType();
    }

    public class ConstrainedTypeContainer<T> : ITypeConstraint<T>
    {
        public Type GetContainingType()
        {
            return typeof(T);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ConstrainedTypeContainer<T> objCasted)) return false;
            return objCasted.GetContainingType() == GetContainingType();
        }

        public override int GetHashCode()
        {
            return GetContainingType().GetHashCode();
        }

        public static implicit operator Type(ConstrainedTypeContainer<T> container)
        {
            return container.GetContainingType();
        }

        public static implicit operator ConstrainedTypeContainer<T>(Type type)
        {
            if (type != typeof(T))
            {
                throw new Exception("Types mismatch exception!");
            }

            return new ConstrainedTypeContainer<T>();
        }
    }
}