﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModulesFramework
{
    public abstract class ModuleLoader<TModule, TData> where TModule : ILoadableModule<TData>
        where TData : BaseModuleLoadData
    {
        private readonly Dictionary<Type, TModule> registeredModules = new Dictionary<Type, TModule>();
        private readonly HashSet<Type> loadedModules = new HashSet<Type>();
        private readonly HashSet<Type> loadingModules = new HashSet<Type>();

        public void RegisterModule(TModule module)
        {
            registeredModules.Add(module.GetType(), module);
        }

        public bool GetInstancedModule(Type type, out TModule module)
        {
            return registeredModules.TryGetValue(type, out module) && loadedModules.Contains(type);
        }

        public void Load(Type type, TData data, Action onLoaded)
        {
            if (loadingModules.Contains(type))
            {
                Debug.LogError("Module is already loading!");
                return;
            }

            if (loadedModules.Contains(type))
            {
                if (data.reloadIfAlreadyLoaded)
                {
                    Reload(type, data, onLoaded);
                }
                else
                {
                    Debug.LogError("Module is already loaded!");
                }

                return;
            }

            if (!registeredModules.TryGetValue(type, out var moduleInstance))
            {
                Debug.LogError("Suitable module not found!");
                return;
            }

            loadingModules.Add(type);
            if (data.garbageCollectionType == BaseModuleLoadData.GCType.BeforeLoad)
            {
                GC.Collect();
            }

            Load(moduleInstance, data, () =>
            {
                moduleInstance.OnModuleLoaded(data, () =>
                {
                    loadingModules.Remove(type);
                    loadedModules.Add(type);
                    if (data.garbageCollectionType == BaseModuleLoadData.GCType.AfterLoad)
                    {
                        GC.Collect();
                    }

                    onLoaded?.Invoke();
                });
            });
        }

        private void Reload(Type type, TData data, Action onLoaded)
        {
            var instance = registeredModules[type];
            instance.OnBeforeModuleUnloaded(() => { instance.OnModuleLoaded(data, onLoaded); });
        }

        public void Unload(Type type, Action onUnloaded)
        {
            if (!loadedModules.Contains(type))
            {
                Debug.LogError("There is no loaded module of type " + type);
                return;
            }

            var moduleInstance = registeredModules[type];
            moduleInstance.OnBeforeModuleUnloaded(() =>
            {
                Unload(moduleInstance, () =>
                {
                    loadedModules.Remove(type);
                    onUnloaded?.Invoke();
                });
            });
        }

        protected abstract void Load(TModule module, TData data, Action callback);
        protected abstract void Unload(TModule module, Action callback);
    }
}